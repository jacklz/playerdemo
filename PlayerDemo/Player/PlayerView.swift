//
//  PlayerView.swift
//  PlayerDemo
//
//  Created by Macbook Pro on 7/11/2020.
//  Copyright © 2020 jacklam. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import RxCocoa
import RxSwift


public enum PlayerState {
    case playing
    case pause
    case buffering
}
struct PlayerDuration {
    var currentTime : Double = 0
    var totalTime : Double = 0
    
    func progress() -> Double{
        return currentTime / totalTime
    }
    init(currentTime:Double,totalTime:Double) {
        self.currentTime = safeCovert(currentTime)
        self.totalTime = safeCovert(totalTime)
    }
    func safeCovert(_ value:Double) -> Double {
        if value.isInfinite || value.isNaN {
            return 0
        }
        return value
    }
}
class PlayerView : UIView {

    let disposeBag = DisposeBag()
    let playerLayer = AVPlayerLayer(player: AVPlayer())
    var timeObserver: Any?
    
    func initPlayerView() {
        
        playerLayer.frame = self.bounds
        self.layer.addSublayer(playerLayer)
        
        PlayerHelper.shared.playVideo.subscribe(onNext: { video in
            self.loadVideo(video)
        }).disposed(by: disposeBag)
        
        PlayerHelper.shared.changeState.subscribe(onNext: { state in
            self.playerChangeState(state)
        }).disposed(by: disposeBag)
    }
    
    func loadVideo(_ video:Video) {

        if let observer = self.timeObserver {
            playerLayer.player?.removeTimeObserver(observer)
        }
        
        let player = AVPlayer(url: video.url)
      
        playerLayer.player = player

        player.play()

        self.timeObserver = player.addPeriodicTimeObserver(forInterval: CMTimeMake(value: 1, timescale: 600), queue: DispatchQueue.main, using: { time in
            if let currentItem = player.currentItem {
                
                let duration = PlayerDuration.init(currentTime: player.currentTime().seconds, totalTime: currentItem.duration.seconds)
                PlayerHelper.shared.duration.accept(duration)
            }
        })

    }
    
    func playerChangeState(_ state:PlayerState) {
        switch state {
        case .playing:
            playerLayer.player?.play()
        case .pause:
            playerLayer.player?.pause()
        default:
            break
        }
    }
    
  
 
   
}

