//
//  PlayerHelper.swift
//  PlayerDemo
//
//  Created by Macbook Pro on 7/11/2020.
//  Copyright © 2020 jacklam. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class PlayerHelper  {
    
    static let shared = PlayerHelper()
    
    var playVideo = PublishRelay<Video>()
    var changeState = PublishRelay<PlayerState>()
    var duration = PublishRelay<PlayerDuration>()
    
}
