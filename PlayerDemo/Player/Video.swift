//
//  Video.swift
//  PlayerDemo
//
//  Created by Macbook Pro on 7/11/2020.
//  Copyright © 2020 jacklam. All rights reserved.
//

import Foundation

struct Video {
    
    var url : URL
    var name : String
    
    init(url:String, name:String) {
        
        self.url = URL.init(string: url)!
        self.name = name
    }
    
    
}
class DemoVideo{
    func getDemoList() -> [Video] {
        return [Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", name: "Big Buck Bunny"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4", name: "ElephantsDream"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4", name: "Gtv Videos Bucket"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4", name: "For Bigger Escapes"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4", name: "For Bigger Joyrides"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4", name: "For Bigger Meltdowns"),
        Video.init(url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4", name: "Sintel"),
        ]
    }
}
