//
//  ViewController.swift
//  PlayerDemo
//
//  Created by Macbook Pro on 7/11/2020.
//  Copyright © 2020 jacklam. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    let disposeBag = DisposeBag()
    let videoList : [Video] = DemoVideo().getDemoList()
        
    var numberOfVideoPlaying : Int = 0

    @IBOutlet var playerView: PlayerView!
    @IBOutlet var playButton: UIButton!
    @IBOutlet var pauseButton: UIButton!
    @IBOutlet var nowPlayingLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initPlayerView()
        playFirstVideo()
        
    }
    
    func initPlayerView() {
        playerView.initPlayerView()
        PlayerHelper.shared.duration.subscribe(onNext:{ duration in
            self.updateDurationUI(duration)
        }).disposed(by:self.disposeBag)
    }

    func playFirstVideo() {
        if let firstVideo = videoList.first {
            numberOfVideoPlaying = 0
            self.playVideo(firstVideo)
        }
    }



    @IBAction func playOnClick(_ sender: Any) {
        resumeVideo()
    }
    @IBAction func pauseOnClick(_ sender: Any) {
        pauseVideo()
    }
    @IBAction func nextOnClick(_ sender: Any) {
        playNextVideo()
    }
    @IBAction func previousOnClick(_ sender: Any) {
        playPreviousVideo()
    }
    
}

