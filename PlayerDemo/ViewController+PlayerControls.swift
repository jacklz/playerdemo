//
//  ViewController+PlayerControls.swift
//  PlayerDemo
//
//  Created by Macbook Pro on 9/11/2020.
//  Copyright © 2020 jacklam. All rights reserved.
//

import Foundation

extension ViewController {
    
    func updateDurationUI(_ duration:PlayerDuration) {
        self.durationLabel.text = "\(Int(duration.currentTime))s/\(Int(duration.totalTime))s"
    }
    
    
    func playVideo(_ video:Video) {
       self.nowPlayingLabel.text = video.name
       PlayerHelper.shared.playVideo.accept(video)
       self.resumeVideo()
    }

    func resumeVideo() {
       self.playButton.isHidden = true
       self.pauseButton.isHidden = false
       PlayerHelper.shared.changeState.accept(.playing)
    }

    func pauseVideo() {
       self.playButton.isHidden = false
       self.pauseButton.isHidden = true
       PlayerHelper.shared.changeState.accept(.pause)
    }

    func playNextVideo() {
       if numberOfVideoPlaying < videoList.count-1 {
           numberOfVideoPlaying += 1
           self.playVideo(videoList[numberOfVideoPlaying])
       }
    }

    func playPreviousVideo() {
       if numberOfVideoPlaying > 0 {
           numberOfVideoPlaying -= 1
          self.playVideo(videoList[numberOfVideoPlaying])
      }
    }
}
